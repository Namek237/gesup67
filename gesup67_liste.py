# -*- coding: utf-8 -*-

import os
import sys
from qgis.core import (
    QgsProject, QgsReadWriteContext, QgsLayoutExporter, 
    QgsLayout, QgsLayoutItemPage, QgsLayoutSize, QgsLayoutPageCollection, 
    QgsLayoutItemHtml, QgsLayoutMeasurement, QgsUnitTypes, QgsLayoutSize, QgsLayoutPoint)
from PyQt5.QtXml import QDomDocument
from PyQt5.QtCore import QRectF
from qgis.utils import iface
from .gesup67_objets import *
import datetime
import os.path
import pandas as pd


# fonction qui génère toutes les liste de sup par commune du departement
def genAllList(path):
    # Recupération de tous les nom de communes
    L = ListesValues()
    res = L.listeNomCom
    # initialisation des pdf
    pdf = Pdf()
    pdf.perimetre = "COMMUNE"
    pdf.outputPath = path
    for commune in res:
        pdf.nom = commune
        query = "SELECT s.id FROM synthese.communes AS com  LEFT JOIN synthese.sup AS s ON POSITION(com.code_comm IN s.code_comm) != 0 WHERE com.nom_comm ILIKE '"+commune+"'"
        pdf.listId = L.getListe(query)[1:]
        pdf.genList()


#################### Objet Pdf ################################################
# qui génère les liste de SUP sélectionnées
class Pdf():
    
    def __init__(self):
        """Constructeur.
        """
        self.erreur = 'Aucune erreur!'
        self.G = Gesup67Connection()
        self.path = str(os.path.dirname(__file__))
        self.outputPath = ''
        
        self.listId = []
        self.perimetre = ''
        self.nom = ''
        self.page = 0
        self.nbPages = 0
        self.data = dict()
        self.listCat = list()
        
    
    # exporte le document en pdf
    def exportToPdf(self, layout, pdfName='temp'):
        exporter = QgsLayoutExporter(layout)
        exporter.exportToPdf(self.path+'/temp/'+pdfName+'.pdf', QgsLayoutExporter.PdfExportSettings())
    
    def loadTemplate(self, path):
        layout = QgsLayout(QgsProject.instance())
        # Load template from file
        with open(path) as f:
            tmplt = f.read()
        doc = QDomDocument()
        doc.setContent(tmplt)
        layout.initializeDefaults()
        items, ok = layout.loadFromTemplate(doc, QgsReadWriteContext(), False)
        return layout
    
    # cree une page du corps et ajoute à la collection de pages
    def editLayout(self, layout, categorie, liste):
        # date du jour
        current_day = datetime.date.today()
        formatted_date = datetime.date.strftime(current_day, "%d/%m/%Y")
        date = "Dressé le :  "+formatted_date
        layout.itemById('date_jour').setText(date)
        # nom du document au pied de page
        nomgeo = "Liste de servitudes d'utilité publique :  "+str(self.perimetre).capitalize()+"  de  "+str(self.nom)
        layout.itemById('nom').setText(nomgeo)
        # categorie en entete de page
        layout.itemById('categorie').setText(categorie)
        designation = self.getDesignation(categorie)
        # designation de la categorie en entete
        layout.itemById('designation').setText(designation)
        # numero de page
        layout.itemById('page').setText("Page "+str(self.page+1)+" / "+str(self.nbPages))
        v = ['nomsup','description','typeacte','date','gestionnaire','adresse']
        for i in range(len(liste)):
            for id in v:
                layout.itemById(id+str(i+1)).setText(liste[i][id])
    
    # remplissage de listCat
    def fillListCat(self):
        if len(self.listId) != 0:
            self.listCat = self.getListCat()
    
    # remplissage du dictionnaire data
    def fillData(self):
        for cat in self.listCat:
            liste = self.supByCat(cat)
            self.data[cat] = liste
    
    # determination du nombre de pages
    def setNbPages(self):
        for liste in self.data.values():
            if len(liste)%6 != 0:
                self.nbPages += (len(liste)//6) + 1
            else:
                self.nbPages += (len(liste)//6)
    
    # crée les pages du document final
    def createPages(self):
        for cat in self.listCat:
            liste = self.data[cat]
            val = []
            for i in range(len(liste)):
                if (i+1)%6 != 0:
                    val.append(liste[i])
                    if i+1 == len(liste):
                        n = (i+1)%6
                        layout = self.loadTemplate(self.path+'/templates/template_corps'+str(n)+'.qpt')
                        self.editLayout(layout, cat, val)
                        self.exportToPdf(layout, 'page'+str(self.page))
                        self.page += 1
                        val = []
                else:
                    val.append(liste[i])
                    layout = self.loadTemplate(self.path+'/templates/template_corps6.qpt')
                    self.editLayout(layout, cat, val)
                    self.exportToPdf(layout, 'page'+str(self.page))
                    self.page += 1
                    val = []
    
    # fusionne les pgf crées
    def merge(self):
        # fusion
        exe = self.path+'/merger/dist/mergePDFs.exe'
        input = '"'+self.path+'/temp/"'
        name = 'SUP_'+self.perimetre+'_'+self.nom
        try:
            os.system('cd '+self.path+'/merger/dist'+' & mergePDFs.exe ' + input + ' "' +self.outputPath+'\\'+name + '" ' + str(self.page))
        except:
            self.erreur = "Erreur: Repertoire de travail introuvable!"
    
    # vide le dossier temp
    def deleteTemp(self):
        # suppression des fichiers inutiles
        files = os.listdir(self.path+'/temp')
        for f in files:
            os.remove(self.path+'/temp/'+f)
        
    # retourne la liste des differentes categories dans les resultats de la recherche
    def getListCat(self):
        query = "SELECT DISTINCT categorie FROM synthese.sup WHERE id IN ("
        l=''
        for id in self.listId:
            l = l+","+str(id)
        query = query + l[1:] + ") ORDER BY categorie ASC"
        res = self.G.getResult(query)
        r = [i[0] for i in res]
        self.erreur = self.G.erreur
        return r
        
    # retourne la designation d'une categorie
    def getDesignation(self, cat):
        query = "SELECT designation FROM synthese.nomenclature_sup WHERE categorie LIKE '"+str(cat)+"'"
        res = self.G.getResult2(query)
        if len(res) != 0:
            return res[0]['designation']
        else:
            return 'Non renseigné'
    
    # retourne les sup d'une categorie dans la selection
    def supByCat(self, categorie):
        l=''
        for id in self.listId:
            l = l+","+str(id)
        query = "WITH s AS (SELECT * FROM synthese.sup WHERE id IN ("+l[1:]+") AND categorie ILIKE '"+categorie+"'),"
        query = query + "foo AS (SELECT s.*, g.nomgest, g.adresse FROM s JOIN synthese.gestionnaires_sup AS g ON s.idgest = g.idgest),"
        query = query + "x AS (SELECT rel.idsup, act.typeacte, act.datedecis FROM synthese.relation_actes_sup AS rel INNER JOIN synthese.actes_sup AS act ON rel.idacte = act.idacte)"
        query = query + "SELECT foo.nomsup, foo.nomsuplitt, foo.descriptio, foo.nomgest, foo.adresse, x.typeacte, x.datedecis FROM foo JOIN x ON x.idsup = foo.idsup GROUP BY foo.nomsup, foo.nomsuplitt, foo.descriptio, foo.nomgest, foo.adresse, x.typeacte, x.datedecis"
        res = self.G.getResult2(query)
        liste = []
        for row in res:
            d = dict()
            if row['nomsuplitt'] == 'Non renseigné':
                d['nomsup'] = row['nomsup']
            else:
                d['nomsup'] = row['nomsuplitt']
            d['description'] = row['descriptio']
            d['typeacte'] = row['typeacte']
            d['date'] = row['datedecis']
            d['gestionnaire'] = row['nomgest']
            d['adresse'] = row['adresse']
            liste.append(d)
        return liste

    # genere la liste en entier
    def genList(self):
        if len(self.listId) != 0:
            # vide temp
            self.deleteTemp()
            # initialisation des parametres du document
            self.fillListCat()
            self.fillData()
            self.setNbPages()
            # creation des pages
            self.createPages()
            # fusionne tous les documents crees
            self.merge()
            # vide a nouveau temp
            self.deleteTemp()
            self.erreur = 'Liste génerée dans : '+self.outputPath
        else:
            self.erreur = "Erreur: Aucune liste générée!"

    # exporte des tableau en csv
    def exportCSV(self, columns, query, fileName='export'):
        # liste de fichiers du repertoire
        liste = os.listdir(self.outputPath)
        if fileName+'.csv' not in liste:
            # resultat de la requete
            data = self.G.getResult(query)
            # transformation en table pandas
            df = pd.DataFrame(data=data,columns=columns)
            # export en csv
            try:
                df.to_csv(self.outputPath+'/'+fileName+'.csv')
            except:
                self.erreur = "Erreur: Repertoire de travail introuvable!"
                pass
    
    # SUPPRIMES LES CSV AJOUT2S
    def delCSV(self,directorie):
        l = ['Nomenlature.csv','Gestionnaires.csv','Communes.csv']
        # suppression des fichiers inutiles
        files = os.listdir(directorie)
        for f in files:
            if f in l:
                try:
                    os.remove(directorie+'/'+f)
                except:
                    self.erreur = "Erreur: Repertoire de travail introuvable!"
                    pass
        