# -*- coding: utf-8 -*-

import os
import sys, time, random
import psycopg2
import psycopg2.extras
from qgis.core import (
    QgsVectorLayer, QgsProject, QgsDataSourceUri, QgsFeatureRequest, QgsWkbTypes, QgsPalLayerSettings, QgsVectorLayerSimpleLabeling
)
from qgis.gui import (QgsMapCanvas)
import processing
from PyQt5.QtGui import QColor
from qgis.utils import iface

# récupérer le chemin du répertoire courant
path = os.getcwd()
sys.path.insert(1, path)

# Supprime toutes les tables temporaires crees dans la base
def deleteTables(id):
    liste = ['sup_communes_','sup_epci_','sup_scot_','sup_dep_','selected_sup_','generateur_s_','generateur_p_','generateur_l_','assiette_s_','assiette_p_','assiette_l_']
    for i in liste:
        query = 'DROP TABLE IF EXISTS temp.'+i+str(id)
        G = Gesup67Connection()
        G.modifierBd(query)
    


################# Objet Params ##############################################
# classe contenant les parametres du plugins
class Params:

    def __init__(self):
        """Constructeur.
        """
        self.erreur = 'Aucune erreur!'
        # parametres de connection a la base de donnees
        self.host = '10.67.1.34'
        self.port = '5432'
        self.user = 'postgres'
        self.password = 'postgres'
        self.base = 'BD_SUP_DRAFT'
        
        # chemin vers le repertoire de travail
        self.path = ''
        
    def setConnectParams(self, host, port, user, password, base):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.base = base
    
    def setWorkspace(self, path):
        self.path = path
        
    
################# Objet ListesValues ########################################
# classe qui genere toutes les listes de donnees qui seront utilisees par le front office
class ListesValues:
    
    def __init__(self):
        """Constructeur.
        """
        self.erreur = 'Aucune erreur!'
        # recuperation des parametres de la base de donnees
        self.Params = Params()
        
        # creation de la connection postgis
        self.G = Gesup67Connection()
        
        # Listes a recuperer par le front office
        self.listeCatSup = self.getDistinct(table="sup", column="categorie", schema="synthese")
        self.listeSousCatSup = self.getDistinct(table="sup", column="sous_categorie", schema="synthese")
        self.listeGest = self.getDistinct(table="gestionnaires_sup", column="nomgest", schema="synthese")
        self.listeTypeActe = self.getDistinct(table="actes_sup", column="typeacte", schema="synthese")
        self.listeNomSup = self.getDistinct(table="sup", column="nomsup", schema="synthese")
        self.listeNomCom = self.getDistinct(table="communes", column="nom_comm", schema="synthese")
        self.listeNomEpci = self.getDistinct(table="epci", column="nom_epci", schema="synthese")
        self.listeNomScot = self.getDistinct(table="scot", column="nom", schema="synthese")
    
    # execute la requete postgres renvoyant la liste recherchee 
    def getListe(self, query):
        liste = self.G.getResult(query)
        liste2 = [''] + [liste[i][0] for i in range(len(liste))]
        return liste2
    
    # retourne la liste de tous les schemas de la base
    def getSchema(self):
        query = "SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT ILIKE '%pg_%' AND schema_name NOT ILIKE '%information_schema%' AND schema_name NOT ILIKE '%topology%'"
        return self.getListe(query)
    
    # retourne la liste de toutes les tables d'un schema
    def getTables(self, schema):
        query = "SELECT table_name FROM information_schema.tables WHERE table_schema ILIKE '"+str(schema)+"' AND table_type='BASE TABLE' ORDER BY table_name"
        return self.getListe(query)
    
    # retourne la liste des colonnes d'une table d'un schema
    def getColumns(self, schema, table):
        query = "SELECT column_name FROM information_schema.columns WHERE table_schema = '"+str(schema)+"' AND table_name  = '"+str(table)+"'"
        return self.getListe(query)
        
    # retourne la liste de toutes les entités distinctes d'une colonne 
    def getDistinct(self, table, column, schema="synthese"):
        query = "SELECT DISTINCT "+column+" FROM "+schema+"."+table+" ORDER BY "+column+" ASC;"
        return self.getListe(query)
        
        
#################   Objet Filtre par localisation ###########################
# Classe qui represente le filtre et effectue les taches du plugin
class Filtres:
    
    def __init__(self):
        """Constructeur.
        """
        self.erreur = 'Aucune erreur!'
        # identifient unique pour la session
        self.id = self.makeUniqueIdent()
        # type de recherche
        self.rechAttr = False
        self.rechLoc = False
        # si recherche par Localisation
        self.com = False
        self.nomCom = ''
        self.epci = False
        self.nomEpci = ''
        self.scot = False
        self.nomScot = ''
        # parametres de sup
        self.catSup = ''
        self.sousCatSup = ''
        self.dateArr = ''
        self.gest = ''
        self.typeActe = ''
        self.statut = ''
        self.dateAbro = ''
        self.nomSup = ''
        self.description = ''
        # Generation des communes ou epci concerne(e)s
        self.genCom = False
        self.genEpci = False
        self.genScot = False
        # Liste des identifients des sup selectionnees
        self.selectedId = list()
        # pour la recherche par periode
        self.period = False
        self.dateEnd = ''
    

    # Génére un identifient aléatoire
    def makeUniqueIdent(self):
        t = hex(int(time.time()*1000))[2:]
        r = hex(random.randint(1000000,10000000))[2:]
        return str(t) + str(r)
    
    # selectionne l'id
    def getId(self, schema, table, colonne, val):
        if val !='':
            value = val
        else:
            value = '%%'
        query = "SELECT id FROM "+schema+"."+table+" WHERE "+colonne+" ILIKE '"+value+"'"
        G = Gesup67Connection()
        r = G.getResult(query)
        return r[0][0]
    
    # crée le schema temporaire s'il n'existe pas deja
    def querySchema(self):
        query = "CREATE SCHEMA IF NOT EXISTS temp"
        return query
    
    # crée une TABLE des sup sur un epci
    def queryEpci(self):
        query = "CREATE TABLE temp.sup_epci_"+str(self.id)+"  AS (WITH c AS (WITH buffer AS (SELECT ST_Buffer(epci.geom,500) AS geom FROM synthese.epci WHERE nom_epci ILIKE '"+str(self.nomEpci).replace("'",'%')+"') SELECT code_comm AS cody FROM synthese.communes AS communes, buffer WHERE ST_Within(communes.geom,buffer.geom)) SELECT tsc.* FROM c, temp.sup_dep_"+str(self.id)+" AS tsc WHERE POSITION(c.cody IN tsc.code_comm) != 0);"
        return query
    
    # crée une TABLE des sup sur une communes
    def queryCom(self):
        query = "CREATE TABLE temp.sup_communes_"+str(self.id)+"  AS (WITH nom AS (SELECT code_comm FROM synthese.communes WHERE nom_comm ILIKE '"+str(self.nomCom).replace("'",'%')+"') SELECT tsc.* FROM temp.sup_dep_"+str(self.id)+" AS tsc, nom WHERE POSITION(nom.code_comm IN tsc.code_comm) != 0);"
        return query
    
    # crée une TABLE des sup sur un scot
    def queryScot(self):
        query = "CREATE TABLE temp.sup_scot_"+str(self.id)+"  AS (WITH c AS (WITH buffer AS (SELECT ST_Buffer(scot.geom,500) AS geom FROM synthese.scot WHERE nom ILIKE '"+str(self.nomScot).replace("'",'%')+"') SELECT code_comm AS cody FROM synthese.communes AS communes, buffer WHERE ST_Within(communes.geom,buffer.geom)) SELECT tsc.* FROM c, temp.sup_dep_"+str(self.id)+" AS tsc WHERE POSITION(c.cody IN tsc.code_comm) != 0);"
        return query
    
    # crée une TABLE de toutes les sup du département
    def queryDep(self):
        query = "CREATE TABLE temp.sup_dep_"+str(self.id)+"  AS ("
        query = query + "WITH x AS (SELECT rel.idsup, act.typeacte, act.datedecis FROM synthese.relation_actes_sup AS rel INNER JOIN synthese.actes_sup AS act ON rel.idacte = act.idacte),"
        query = query + " y AS (SELECT tsc.*, gest.nomgest  FROM synthese.sup AS tsc INNER JOIN synthese.gestionnaires_sup AS gest ON tsc.idgest = gest.idgest)"
        query = query + " SELECT y.*, x.typeacte, x.datedecis FROM y INNER JOIN x ON y.idsup = x.idsup"
        query = query + " GROUP BY y.id, y.partition, y.idsup, y.idgest, y.nomsup, y.nomsuplitt, y.commentaire, y.categorie, y.idintgest, y.descriptio, y.datemaj, y.validegest, y.obsvalidat, y.estabroge, y.modeprod, y.quiprod, y.docsource, y.code_comm, y.sous_categorie, y.date_abrogation, y.source, y.nomgest, x.typeacte, x.datedecis"
        query = query + ");"
        return query
    
    # crée la TABLE attributaire des SUP à partir d'une des tables ci-dessus
    def queryAttSup(self):
        query = "CREATE TABLE temp.selected_sup_"+str(self.id)+" AS (SELECT sup.* FROM temp.sup_"
        if self.rechAttr:
            query = query+"dep"
        elif self.rechLoc:
            if self.com:
                query = query+"communes"
            elif self.epci:
                query = query+"epci"
            elif self.scot:
                query = query+"scot"
            else:
                query = query+"dep"
        query = query + "_" + str(self.id) + " AS sup WHERE ( "
        if str(self.catSup) != '':
            query = query + "categorie ILIKE '"+str(self.catSup)+"' "
        else:
            query = query + "categorie ILIKE '%"+str(self.catSup)+"%' "
        if str(self.sousCatSup) != '':
            query = query + "AND sous_categorie ILIKE '"+str(self.sousCatSup)+"' "
        if str(self.dateArr) != '':
            if self.period and len(str(self.dateEnd)) == 10:
                query = query + "AND LENGTH(SUBSTRING(datedecis, LENGTH(datedecis)-10, 11)) = 10"
                query = query + "AND TO_DATE(SUBSTRING(datedecis, LENGTH(datedecis)-10, 11),'DD/MM/YYYY') >= TO_DATE('"+str(self.dateArr)+"','DD/MM/YYYY') "
                query = query + "AND TO_DATE(SUBSTRING(datedecis, LENGTH(datedecis)-10, 11),'DD/MM/YYYY') <= TO_DATE('"+str(self.dateEnd)+"','DD/MM/YYYY') "
            else:
                query = query + "AND datedecis ILIKE '%"+str(self.dateArr)+"%' "
        if str(self.gest) != '':
            query = query + "AND nomgest ILIKE '"+str(self.gest)+"' "
        if str(self.typeActe) != '':
            query = query + "AND typeacte ILIKE '"+str(self.typeActe)+"' "
        if str(self.statut) != '':
            query = query + "AND estabroge ILIKE  '"+str(self.statut)+"' "
        if str(self.dateAbro) != '':
            query = query + "AND date_abrogation ILIKE '"+str(self.dateAbro)+"' "
        if str(self.description) != '':
            query = query + "AND descriptio ILIKE '%"+str(self.description)+"%' "
        if str(self.nomSup) != '':
            query = query + "AND nomsup ILIKE '%"+str(self.nomSup)+"%'"
        query = query + " ));"
        return query
    
    # erifie si aucun parametre n'a été entré
    def noFilter(self):
        if self.rechAttr:
            x = str(self.dateEnd)+str(self.catSup)+str(self.sousCatSup)+str(self.dateArr)+str(self.gest)+str(self.typeActe)+str(self.statut)+str(self.dateAbro)+str(self.nomSup)+str(self.description)
            if x == '':
                return True
            else:
                return False
        
        
    # crée des tables temporaires de générateurs
    def queryGen(self):
        if not self.noFilter():
            query1 = "CREATE TABLE temp.generateur_s_"+str(self.id)+"  AS (SELECT gen.* FROM synthese.generateur_s AS gen, temp.selected_sup_"+str(self.id)+" AS sup WHERE sup.idsup = gen.idsup AND sup.categorie ILIKE gen.suptype);"
            query2 = "CREATE TABLE temp.generateur_l_"+str(self.id)+"  AS (SELECT gen.* FROM synthese.generateur_l AS gen, temp.selected_sup_"+str(self.id)+" AS sup WHERE sup.idsup = gen.idsup AND sup.categorie ILIKE gen.suptype);"
            query3 = "CREATE TABLE temp.generateur_p_"+str(self.id)+"  AS (SELECT gen.* FROM synthese.generateur_p AS gen, temp.selected_sup_"+str(self.id)+" AS sup WHERE sup.idsup = gen.idsup AND sup.categorie ILIKE gen.suptype);"
        else:
            query1 = "CREATE TABLE temp.generateur_s_"+str(self.id)+"  AS (SELECT gen.* FROM synthese.generateur_s AS gen);"
            query2 = "CREATE TABLE temp.generateur_l_"+str(self.id)+"  AS (SELECT gen.* FROM synthese.generateur_l AS gen);"
            query3 = "CREATE TABLE temp.generateur_p_"+str(self.id)+"  AS (SELECT gen.* FROM synthese.generateur_p AS gen);"
        query = query1 + query2 + query3
        return query

    # crée une table temporaire d'assiettes
    def queryAss(self):
        if not self.noFilter():
            query1 = "CREATE TABLE temp.assiette_s_"+str(self.id)+"  AS (SELECT ass.* FROM synthese.assiette_s AS ass, temp.generateur_s_"+str(self.id)+" AS gen WHERE gen.idgen = ass.idgen  AND ass.suptype ILIKE gen.suptype);"
            query2 = "CREATE TABLE temp.assiette_l_"+str(self.id)+"  AS (SELECT ass.* FROM synthese.assiette_l AS ass, temp.generateur_l_"+str(self.id)+" AS gen WHERE gen.idgen = ass.idgen  AND ass.suptype ILIKE gen.suptype);"
            query3 = "CREATE TABLE temp.assiette_p_"+str(self.id)+"  AS (SELECT ass.* FROM synthese.assiette_p AS ass, temp.generateur_p_"+str(self.id)+" AS gen WHERE gen.idgen = ass.idgen  AND ass.suptype ILIKE gen.suptype);"
        else:
            query1 = "CREATE TABLE temp.assiette_s_"+str(self.id)+"  AS (SELECT ass.* FROM synthese.assiette_s AS ass);"
            query2 = "CREATE TABLE temp.assiette_l_"+str(self.id)+"  AS (SELECT ass.* FROM synthese.assiette_l AS ass);"
            query3 = "CREATE TABLE temp.assiette_p_"+str(self.id)+"  AS (SELECT ass.* FROM synthese.assiette_p AS ass);"
        query = query1 + query2 + query3
        return query

    # sélectionne les communes , scot ou epci (ou inclusif) concernés par une ou plusieurs sup
    def querySelectCom(self):
        query = "WITH code AS (SELECT DISTINCT code_comm FROM temp.selected_sup_"+str(self.id)+") SELECT DISTINCT insee_com, nom_comm,  adresse_communale FROM synthese.communes AS com, code WHERE (POSITION(com.code_comm IN code.code_comm) != 0)  ORDER BY nom_comm ASC; "
        return query
    
    def querySelectEpci(self):
        query = "WITH com AS (WITH code AS (SELECT DISTINCT code_comm FROM temp.selected_sup_"+str(self.id)+") SELECT DISTINCT com0.geom FROM synthese.communes AS com0, code WHERE (POSITION(com0.code_comm IN code.code_comm) != 0))"
        query = query + ", buffer AS (SELECT ST_Buffer(com.geom,-200) AS geom FROM com) "
        query = query + "SELECT DISTINCT  id_epci, nom_epci, siren_epci FROM synthese.epci AS ep, buffer WHERE ST_Within(buffer.geom,ep.geom) ORDER BY nom_epci ASC; "
        return query
    
    def querySelectScot(self):
        query = "WITH com AS (WITH code AS (SELECT DISTINCT code_comm FROM temp.selected_sup_"+str(self.id)+") SELECT DISTINCT com0.geom FROM synthese.communes AS com0, code WHERE (POSITION(com0.code_comm IN code.code_comm) != 0))"
        query = query + ", buffer AS (SELECT ST_Buffer(com.geom,-200) AS geom FROM com) "
        query = query + "SELECT DISTINCT id_scot, nom FROM synthese.scot AS sc, buffer WHERE ST_Within(buffer.geom,sc.geom) ORDER BY nom ASC; "
        return query

    def querySelectSup(self):
        return "SELECT id, partition, idsup, idgest, nomsup, nomsuplitt, commentaire, categorie, idintgest, descriptio, datemaj, validegest, obsvalidat, estabroge, modeprod, quiprod, docsource, code_comm, sous_categorie, date_abrogation, source, nomgest, typeacte, datedecis FROM temp.selected_sup_"+str(self.id)+" GROUP BY id, partition, idsup, idgest, nomsup, nomsuplitt, commentaire, categorie, idintgest, descriptio, datemaj, validegest, obsvalidat, estabroge, modeprod, quiprod, docsource, code_comm, sous_categorie, date_abrogation, source, nomgest, typeacte, datedecis"
    
    # Methode qui retourne les resultats des requêtes demandées
    def getResults(self):
        # creation des tables
        self.createTables()
        rCom, rEpci, rScot = [], [], []
        G = Gesup67Connection()
        rSup = G.getResult2(self.querySelectSup())
        if self.genCom:
            rCom = G.getResult2(self.querySelectCom())
        if self.genEpci:
            rEpci = G.getResult2(self.querySelectEpci())
        if self.genScot:
            rScot = G.getResult2(self.querySelectScot())
        self.erreur = G.erreur
        return rSup, rCom, rEpci, rScot
    
    # affiches les couches
    def showLayers(self):
        self.removeAllSelection()
        self.deleteLayers()
        self.createMaskLayers()
        self.createSupLayer()
        
    # Cree les tables temporaires dans la base    
    def createTables(self):
        # avant de créer de nouvelles tables, on supprime les anciennes
        deleteTables(self.id)
        # creation de la connection postgis
        G = Gesup67Connection()
        # crée le schema s'il n'existe pas deja
        G.modifierBd(self.querySchema())
        # crée une table des sup du département 
        G.modifierBd(self.queryDep())
        if self.rechLoc:
            # scot, communes ou epci
            if self.com:
                G.modifierBd(self.queryCom())
            elif self.epci:
                G.modifierBd(self.queryEpci())
            elif self.scot:
                G.modifierBd(self.queryScot())
        # crée une sous-sélection de la table attributaire des sup à partir des données du filtre 
        G.modifierBd(self.queryAttSup())
        # crée les tables de générateurs et assiettes
        G.modifierBd(self.queryGen())
        G.modifierBd(self.queryAss())

    # creation des couches qui seront affichéés sur qgis 
    def createMaskLayers(self):
        # Supprime toutes les etiquettes
        self.removeLabels()
        # couche de masque
        if self.com and len(QgsProject.instance().mapLayersByName('COMMUNES')) == 0:
            communes = Gesup67Layer('synthese', 'communes','COMMUNES')
            communes.setColor("silver")
            communes.addLabel()
            communes.layer.selectByExpression('"id"='+str(self.getId('synthese','communes','nom_comm',str(self.nomCom).replace("'",'%'))))
            communes.addLayer()
        elif self.epci and len(QgsProject.instance().mapLayersByName('EPCI')) == 0:
            communes = Gesup67Layer('synthese', 'communes','COMMUNES')
            communes.setColor("gray")
            communes.addLayer()
            epci = Gesup67Layer('synthese', 'epci','EPCI')
            epci.setColor("cyan")
            epci.addLabel()
            epci.layer.selectByExpression('"id"='+str(self.getId('synthese','epci','nom_epci',str(self.nomEpci).replace("'",'%'))))
            epci.addLayer()
        elif self.scot and len(QgsProject.instance().mapLayersByName('SCOT')) == 0:
            communes = Gesup67Layer('synthese', 'communes','COMMUNES')
            communes.setColor("gray")
            communes.addLayer()
            scot = Gesup67Layer('synthese', 'scot','SCOT')
            scot.setColor("pink")
            scot.addLabel()
            scot.layer.selectByExpression('"id"='+str(self.getId('synthese','scot','nom',str(self.nomScot).replace("'",'%'))))
            scot.addLayer()
        elif len(QgsProject.instance().mapLayersByName('COMMUNES'))==0 and len(QgsProject.instance().mapLayersByName('EPCI')) == 0 and len(QgsProject.instance().mapLayersByName('SCOT')) == 0:
            communes = Gesup67Layer('synthese', 'communes','COMMUNES')
            communes.setColor("gray")
            if self.com:
                communes.layer.selectByExpression('"id"='+str(self.getId('synthese','communes','nom_comm',str(self.nomCom).replace("'",'%'))))
            communes.addLayer()
            
    def createSupLayer(self):
        # générateurs et assiettes surfaciques
        ass_s = Gesup67Layer('temp', 'assiette_s_'+str(self.id), 'ASSIETTES_SURF')
        if ass_s.hasFeatures():
            ass_s.addLayer()
        gen_s = Gesup67Layer('temp', 'generateur_s_'+str(self.id), 'GENERATEURS_SURF')
        if gen_s.hasFeatures():
            gen_s.addLayer()
        # générateurs et assiettes linéaires
        ass_l = Gesup67Layer('temp', 'assiette_l_'+str(self.id), 'ASSIETTES_LIN')
        if ass_l.hasFeatures():
            ass_l.addLayer()
        gen_l = Gesup67Layer('temp', 'generateur_l_'+str(self.id), 'GENERATEURS_LIN')
        if gen_l.hasFeatures():
            gen_l.addLayer()
        # générateurs et assiettes ponctuels
        ass_p = Gesup67Layer('temp', 'assiette_l_'+str(self.id), 'ASSIETTES_PONC')
        if ass_p.hasFeatures():
            ass_p.addLayer()
        gen_p = Gesup67Layer('temp', 'generateur_p_'+str(self.id), 'GENERATEURS_PONC')
        if gen_p.hasFeatures():
            gen_p.addLayer() 

    # suppression les couches de gen et ass du projet QGIS
    def deleteLayers(self):
        layersToRemove = ['GENERATEURS_PONC','GENERATEURS_LIN','GENERATEURS_SURF','ASSIETTES_SURF','ASSIETTES_LIN','ASSIETTES_PONC','EPCI','SCOT','COMMUNES']
        layers = QgsProject.instance().mapLayers().items()
        for id, layer in layers:
            if layer.name() in layersToRemove:
                QgsProject.instance().removeMapLayer(layer.id())
    
    # suppression toutes les couches du projet QGIS
    def deleteAllLayers(self):
        QgsProject.instance().removeAllMapLayers()
    
    # rétir l'étiquette sur la couche
    def removeLabels(self):
        label_settings = QgsPalLayerSettings()
        layersToRemove = ['COMMUNES','EPCI','SCOT']
        layers = QgsProject.instance().mapLayers().items()
        for id, layer in layers:
            if layer.name() in layersToRemove:
                layer.setLabeling(QgsVectorLayerSimpleLabeling(label_settings))
                layer.triggerRepaint()
                
    def removeAllSelection(self):
        layersToRemove = ['GENERATEURS_PONC','GENERATEURS_LIN','GENERATEURS_SURF','ASSIETTES_SURF','ASSIETTES_LIN','ASSIETTES_PONC','EPCI','SCOT','COMMUNES']
        layers = QgsProject.instance().mapLayers().items()
        for id, layer in layers:
            if layer.name() in layersToRemove:
                layer.removeSelection()
        
################## Objet connection #########################################
# classe cree la connection a la base de donnees postgis
class Gesup67Connection:
    
    def __init__(self):
        """Constructeur.
        """
        self.erreur = 'Opération réussie!'
        # initialiser les parametre de connexion a la base de données
        self.Params = Params()
        self.host = self.Params.host
        self.port = self.Params.port
        self.dbName = self.Params.base
        self.userName = self.Params.user
        self.password = self.Params.password
        self.connect()

    # crée la variable connect utilisée pour executer des requêtes
    def connect(self):
        try:
            conn = psycopg2.connect(host = self.host, 
                            port = self.port, 
                            user = self.userName, 
                            password = self.password, 
                            database = self.dbName)
            self.connection = conn
        except Exception as e:
            self.erreur = "Exception TYPE:"+str(e)

    
    # execute la requête donnée en parametre
    def modifierBd(self, Query):
        self.connect()
        try:
            cursor = self.connection.cursor()
            cursor.execute(Query)
            self.connection.commit()
        except Exception as e:
            self.erreur = "Exception TYPE:"+str(e)
            

    # execute la requête donnée en parametres et retourne un resultat
    def getResult(self, Query):
        self.connect()
        try:
            cursor = self.connection.cursor()
            cursor.execute(Query)
            data = cursor.fetchall()
            return data
        except Exception as e:
            self.erreur = "Exception TYPE:"+str(e)
        
    
    # execute la requête donnée en parametres et retourne un resultat sous forme de liste de dictionnaires
    def getResult2(self, Query):
        self.connect()
        try:
            cursor = self.connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
            cursor.execute(Query)
            data = cursor.fetchall()
            return data
        except Exception as e:
            self.erreur = "Exception TYPE:"+str(e)

        
        
######################## Objet Action ########################################
# classe permettant d'effectuer une action sur la base
class Action:
    def __init__(self):
        """Constructeur.
        """
        self.erreur = "Opération réussie!"
        # creation de la connection postgis
        self.G = Gesup67Connection()
        
        self.schema = "synthese"
        self.table = ""
        self.dictColId = {'sup':'idsup',
                          'gestionnaires_sup':'idgest',
                          'actes_sup':'idacte',
                          'nomenclature_sup':'categorie',
                          'generateur_l':'idgen',
                          'generateur_s':'idgen',
                          'generateur_p':'idgen',
                          'assiette_l':'idass',
                          'assiette_s':'idass',
                          'assiette_p':'idass',
                          'relation_actes_sup':'idsup'}
    
    # permet de remplacer null par ''
    def correction(self):
        if str(self.table) not in []:
            L = ListesValues()
            t = L.getColumns(str(self.schema),str(self.table))
            toRemove = ['id']
            for i in toRemove:
                try:
                    t.remove(i)
                except:
                    self.erreur = "Erreur dans: correction - Action - gesup67objet"
            for column in t[1:]:
                query = "UPDATE "+str(self.schema)+"."+str(self.table)+" SET "+column+" = 'Non renseigné' WHERE "+column+" IS null"
                self.G.modifierBd(query)
    
    # fonction qui recherche les doublons dans la table
    def doublon(self,id):
        if str(self.table) not in ['communes','epci','scot']:
            column = self.dictColId[str(self.table)]
            query = "SELECT * FROM "+str(self.schema)+"."+str(self.table)+" WHERE "+column+" = '"+str(id)+"'"
            if len(self.G.getResult(query)) != 0:
                return True
            else:
                return False
        else:
            return False
        
        
    # crée une nouvelle ligne dans la table avec les valeurs correspondantes
    def addRow(self, columns, values, geom=None):
        if len(columns) == len(values) and len(values) != 0:
            z = '' 
            query = "INSERT INTO "+str(self.schema)+"."+str(self.table)+" (id," 
            for j in range(len(columns)):
                query = query + columns[j]
                if j != len(columns)-1:
                    query = query + ","
                # recuperation de l'id pour vérifier s'il y a doublon
                if columns[j] == self.dictColId[str(self.table)]:
                    z = values[j]
                # recuperation de idacte et idsup dans le cas de la table sup
                if str(self.table) == "sup":
                    if columns[j] == "idsup":
                        x = values[j]
                    y = ''
                    if columns[j] == "idacte":
                        y = values[j]
            # ajout d'une ligne dans relation_actes_sup
            if str(self.table) == "sup" and y != '':
                query_ = "INSERT INTO "+str(self.schema)+".relation_actes_sup (id,idsup,idacte) VALUES(DEFAULT,'"+str(x)+"','"+str(y)+"')"
                self.G.modifierBd(query_)
            
            # ajout de la colonne geometrique et la suite
            if geom != None:
                query = query + ",geom"
            query = query + ") VALUES (DEFAULT"
            for i in values:
                query = query + ",'"+str(i)+"'"
            if geom != None:
                query = query + ",ST_GeomFromText('" +geom.asWkt()+"',2154)"
            query = query + ");"
            # verifie s'il y a doublon
            if not self.doublon(z):
                self.G.modifierBd(query)
            else:
                self.erreur = "Erreur: Doublon détecté!\nIdentifiant deja présent."
        else:
            self.erreur = "Erreur: Le formulaire est vide!"
    
    # supprime une ou plusieurs lignes à partir de leurs identifients et le nom de la colonne identifient
    def delRows(self, listeId):
        self.erreur = "Opération réussie!"
        if len(listeId)!=0:
            l=''
            for id in listeId:
                l = l+","+str(id)
            query = "DELETE FROM "+str(self.schema)+"."+str(self.table)+" WHERE id IN ("+l[1:]+");"
            self.G.modifierBd(query)
            self.correction()
        else:
            self.erreur = "Erreur: aucune ligne sélectionnée!"
        
    # modifie une ou plusieurs lignes d'une table en affectant la même valeur
    def editRows(self, listeId, columns, values):
        if len(listeId)!=0:
            l=''
            for id in listeId:
                l = l+","+str(id)
            query = "UPDATE "+str(self.schema)+"."+str(self.table)+" SET "
            for i in range(len(columns)-1):
                query = query +columns[i]+ " = '" +values[i]+ "',"
                # recuperation de l'id pour vérifier s'il y a doublon
                if columns[i] == self.dictColId[str(self.table)]:
                    z = values[i]
            query = query +columns[-1]+ " = '" +values[-1]+"' "
            query = query + "WHERE id IN ("+l[1:]+");"
            self.G.modifierBd(query)
            self.correction()
            self.erreur = "Opération réussie!"
        else:
            self.erreur = "Erreur: aucune ligne sélectionnée!"
    
    # ajouter des données depuis une couches qgis existente
    def addFromLayer(self, layerName, dictColumns):
        self.delAllRows()
        # on recupère la couche grace à son nom
        layer = QgsProject.instance().mapLayersByName(layerName)[0]
        features = layer.getFeatures()
        for feature in features:
            if not feature.hasGeometry():
                geom = None
            else:
                # fetch geometry
                geom = feature.geometry()
            # fetch attributes
            attrs = []
            column = []
            for k, v in dictColumns.items():
                if v != '':
                    attrs.append(feature[v])
                    column.append(k)
            # ajout de la ligne dans la table postgis correspondante
            self.addRow(column,attrs,geom)
        # correction de la table en remplacant null par non renseigné
        self.correction()
        # mise a jour des codes comm dans la table des communes
        if str(self.table) == "communes":
            self.setCode()
        # mise a jour des codes comm dans la table des sup
        x = ['communes','generateur_l', 'generateur_p','generateur_s','assiette_s','assiette_p','assiette_l']
        if str(self.table) in x:
            self.setCodeComm()
        if str(self.erreur) == "Erreur: Doublon détecté!\nIdentifiant deja présent.":
            self.erreur = "Alerte: le(s) doublon(s) détecté(s)! n'ont pas été ajouté(s)!"
     
    # fonction qui crée le code_comm de la table des communes a partir du code insee
    def setCode(self):
        query = "UPDATE synthese.communes SET code_comm = RIGHT(insee_com,3)"
        self.G.modifierBd(query)
    
    # interaction de la selection d'une sup dans le tableau avec l'entite correspondante dans la couche
    def select(self,id,rechLoc):
        if not rechLoc:
            layersToUse = ['GENERATEURS_PONC','GENERATEURS_LIN','GENERATEURS_SURF']
            layers = QgsProject.instance().mapLayers().items()
            idsup = self.G.getResult("SELECT idsup FROM synthese.sup WHERE id = "+str(id))[0][0]
            for i, layer in layers:
                if layer.name() in layersToUse:
                    layer.selectByExpression('"idsup"='+idsup)
                    iface.mapCanvas().setSelectionColor( QColor("yellow") )
    
    # selectionne l'entite dans la couche de masque
    def selectMask(self, listeId, rechLoc, nom):
        if not rechLoc:
            layersToUse = {'COMMUNES': 'insee_com','EPCI': 'id_epci','SCOT': 'id_scot'}
            layers = QgsProject.instance().mapLayersByName(nom)
            if len(layers) != 0:
                layer = layers[0]
                l=''
                for id in listeId:
                    l = l+",'"+str(id)+"'"
                layer.selectByExpression('"'+layersToUse[nom]+'" IN ('+l[1:]+')')

    # determine le/les code(s)_comm d'une sup disposant d'une assiette dans la base
    def setCodeComm(self):
        r = self.G.getResult("SELECT idsup, categorie FROM synthese.sup WHERE (code_comm LIKE '' OR code_comm LIKE 'Non renseigné') AND source NOT LIKE 'ACCESS'")
        for s in r:
            idsup = s[0]
            cat = s[1]
            # recherche des generateurs
            idgens = self.G.getResult("SELECT idgen FROM synthese.generateur_s WHERE idsup like '"+str(idsup)+"' AND suptype ILIKE '"+str(cat)+"'")
            idgenl = self.G.getResult("SELECT idgen FROM synthese.generateur_l WHERE idsup like '"+str(idsup)+"' AND suptype ILIKE '"+str(cat)+"'")
            idgenp = self.G.getResult("SELECT idgen FROM synthese.generateur_p WHERE idsup like '"+str(idsup)+"' AND suptype ILIKE '"+str(cat)+"'")
            idgen, let = [], ''
            if len(idgens)!=0:
                for k in range(len(idgens)):
                    idgen.append(idgens[k][0])
                let = 's'
            elif len(idgenl)!=0:
                for k in range(len(idgenl)):
                    idgen.append(idgenl[k][0])
                let = 'l'
            elif len(idgenp)!=0:
                for k in range(len(idgenp)):
                    idgen.append(idgenp[k][0])
                let = 'p'
                
            # recherche de l'assiette de chaque générateur trouvé
            t = ""
            res = []
            if len(idgen) != 0:
                for j in idgen:
                    query = "WITH g AS ("
                    a = "SELECT geom FROM synthese.assiette_"
                    c = " WHERE idgen LIKE '"+j+"' AND suptype ILIKE '"+str(cat)+"'"
                    d = ")SELECT code_comm FROM synthese.communes AS c, g WHERE ST_Intersects(g.geom, c.geom);"
                    e = "UPDATE synthese.sup SET code_comm = '"
                    f = "' WHERE idsup LIKE '"+idsup+"';"
                    query = query + a + let + c + d
                    res = self.G.getResult(query)
                    # s'il n'y a pas d'assiette alors on cherche le generateur
                    if len(res) == 0:
                        query = query.replace('assiette_','generateur_')
                        res = self.G.getResult(query)
                    for o in range(len(res)):
                        t = t+res[o][0]
                        if o != len(res)-1:
                            t = t+","
                    self.G.modifierBd(e+t+f)
    
    def delAllRows(self):
        tablesAllowed = ['communes','scot','epci']
        if str(self.table) in tablesAllowed:
            query = "DELETE FROM "+str(self.schema)+"."+str(self.table)
            self.G.modifierBd(query)
    
    # affiche ou masque une couche QGIS
    def setVisibility(self, name, values):
        layer = QgsProject.instance().mapLayersByName(name)
        qgis.utils.iface.legendInterface().setLayerVisible(layer, values)
                
    
######################## Objet layer ########################################
# classe qui cree et ajouter les couches dans qgis
class Gesup67Layer:
    
    def __init__(self, schema='',table='',layerName='',clauseWhere=''):
        """Constructeur.
        """

        # initialiser les parametre de connexion a la base de données
        self.params = Params()
        # initialiser l'adresse de la TABLE postgis
        self.schema = schema
        self.table = table
        self.clauseWhere = clauseWhere
        self.layerName = layerName  # nom dans la legende qgis
        self.layer = self.createLayer()
    
        
    # Creer le couche qgis
    def createLayer(self):
        uri = QgsDataSourceUri()
        # set host name, port, database name, username AND password
        uri.setConnection(self.params.host, self.params.port, self.params.base, self.params.user, self.params.password)
        # si la couche est géometrique
        if "geom" in ListesValues().getColumns(self.schema, self.table) or str(self.table) == 'communes':
            # set database schema, TABLE name, geometry column AND optionally
            # subset (WHERE clause)
            uri.setDataSource(self.schema, self.table, "geom", self.clauseWhere)
        else:
            # If your using a filter ON TABLE with no geometry
            uri.setDataSource(self.schema, self.table, None, self.clauseWhere)
        # Affectation de la couche crée dans l'attribut de la classe
        if self.layerName == '':
            self.layerName = self.table
        return QgsVectorLayer(uri.uri(False), self.layerName, "postgres")

    # Méthode qui ajoute la couche créée au projet qgis
    def addLayer(self):
        if self.layer.isValid():
            QgsProject.instance().addMapLayer(self.layer)
        

    
    # ajouter un étiquette sur la couche
    def addLabel(self):
        d = {'communes': 'nom_comm', 'epci': 'id_epci', 'scot': 'nom'}
        if str(self.table) in list(d.keys()):
            label_settings = QgsPalLayerSettings()
            label_settings.fieldName = d[str(self.table)] 
            label_settings.fontSizeInMapUnits = False
            label_settings.textColor = QColor(0,0,0) 
            self.layer.setLabelsEnabled(True)
            self.layer.setLabeling(QgsVectorLayerSimpleLabeling(label_settings))
            self.layer.triggerRepaint()
    
    # retourne vrai si la couche est vide et non sinon
    def hasFeatures(self):
        request = QgsFeatureRequest().setFlags(QgsFeatureRequest.NoGeometry).setNoAttributes()
        myIt = self.layer.getFeatures(request)
        try:
            feat = next(myIt)
        except StopIteration:
            feat = False
        return feat
    
    def setColor(self, color):
        self.layer.renderer().symbol().setColor(QColor(color))
        self.layer.triggerRepaint()
    
#################   Objet Filtre scondaire ###########################
class FilterEdit:
    
    def __init__(self):
        """Constructeur.
        """
        self.schema = 'synthese'
        self.table = ''
        self.dictValues = {}
        self.listId = []
    
    def query(self):
        query = "SELECT * FROM "+str(self.schema)+'.'+str(self.table)+" WHERE ("
        i = 0
        for key in list(self.dictValues.keys()):
            query = query +key+" ILIKE '%"+self.dictValues[key]+"%'"
            i = i + 1
            if i != len(self.dictValues):
                query = query + " AND "
        query = query + ");"
        return query
    
    # Methode qui retourne les resultats du filtre
    def getResults(self):
        G = Gesup67Connection()
        if self.table != "sup":
            rSup = G.getResult2(self.query())
        else:
            F = Filtres()
            keys = self.dictValues.keys()
            F.rechAttr = True
            if self.dictValues['nom_comm'] != '':
                F.rechAttr = False
                F.rechLoc = True
                F.com = True
                F.nomCom = self.dictValues['nom_comm']
            F.catSup = self.dictValues['categorie']
            F.sousCatSup = self.dictValues['sous_categorie']
            F.dateArr = self.dictValues['datedecis']
            F.gest = self.dictValues['nomgest']
            F.statut = self.dictValues['estabroge']
            F.dateAbro = self.dictValues['date_abrogation']
            F.nomSup = self.dictValues['nomsuplitt']
            F.nomSup = self.dictValues['descriptio']
            G.modifierBd(F.querySchema())
            G.modifierBd(F.queryDep())
            G.modifierBd(F.queryCom())
            G.modifierBd(F.queryAttSup())
            #requete speciale
            query = "SELECT id,partition,idsup,idgest,nomsup,nomsuplitt,categorie,idintgest,descriptio,datemaj,validegest,obsvalidat,estabroge,modeprod,quiprod,docsource,code_comm,sous_categorie,date_abrogation,source,commentaire FROM temp.selected_sup_"+str(F.id)
            rSup = G.getResult2(query)
            # suppression des tables inutiles
            liste = ['sup_communes_','selected_sup_','sup_dep_']
            for i in liste:
                G.modifierBd('DROP TABLE IF EXISTS temp.'+i+str(F.id))
        return rSup

################ Objet Archive ##########################################
# classe qui se chargera de faire des sauvegardes de mogifications effectuées aucours de la session
class Archive:
    
    def __init__(self):
        """Constructeur.
        """
        self.erreur = "Restauration réussie!"
        self.G = Gesup67Connection()
        self.log = list()
        self.listTable = []
        self.typeOperation = [] # 1 pour modif et 2 pour suppression
    
    # methode qui enregistre une ligne editée dans la structure de données    
    def save(self, table, id, op):
        query = "SELECT * FROM synthese."+table+" WHERE id = "+str(id)
        r = self.G.getResult(query)
        if len(r) != 0:
            self.log.append(r[0])
            self.listTable.append(table)
            self.typeOperation.append(op)
        
    # methode qui restaure toutes les modification apportées à la base
    def restore(self):
        if len(self.listTable) != 0:
            A = Action()
            L = ListesValues()
            A.table = self.listTable[-1]
            columns = L.getColumns('synthese',self.listTable[-1])[2:]
            # restauration n-1
            ligne = self.log[-1]
            if self.typeOperation[-1] == 1:
                A.editRows([ligne[0]], columns, ligne[1:])
            elif self.typeOperation[-1] == 2:
                A.addRow(columns,ligne[1:])
            # suppression apres restauration
            self.log.pop(-1)
            self.listTable.pop(-1)
            self.typeOperation.pop(-1)
            self.erreur = "Restauration réussie!"
        else:
            self.erreur = "Aucune restauration!"